from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.kernel_approximation import RBFSampler
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

boston = datasets.load_breast_cancer()
x = boston.data
y = boston.target
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.4)

# klasyfikator 1: drzewo decyzyjne
classifier1 = tree.DecisionTreeClassifier()
classifier1.fit(x_train, y_train)
predictions1 = classifier1.predict(x_test)
matrix1 = confusion_matrix(y_test, predictions1)
print("decision tree accuracy: " + str(accuracy_score(y_test, predictions1)))
print("decision tree matrix: " + str(matrix1))

# klasyfikator 2: k-najblizszych sąsiadów
classifier2 = KNeighborsClassifier(n_neighbors=3)
classifier2.fit(x_train, y_train)
predictions2 = classifier2.predict(x_test)
matrix2 = confusion_matrix(y_test, predictions2)
print("k-neighbors accuracy: " + str(accuracy_score(y_test, predictions2)))
print("k-neighbors matrix: " + str(matrix2))

# klasyfikator 3: naive bayer
classifier3 = GaussianNB()
classifier3.fit(x_train, y_train)
predictions3 = classifier3.predict(x_test)
matrix3 = confusion_matrix(y_test, predictions3)
print("naive bayer accuracy: " + str(accuracy_score(y_test, predictions3)))
print("naive bayer matrix: " + str(matrix3))

# klasyfikator 4: neural_networks
classifier4 = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
classifier4.fit(x_train, y_train)
predictions4 = classifier4.predict(x_test)
matrix4 = confusion_matrix(y_test, predictions4)
print("neural networks accuracy: " + str(accuracy_score(y_test, predictions4)))
print("neural networks matrix: " + str(matrix4))

# klasyfikator 5: kernel_approximation
rbf_feature = RBFSampler(gamma=1, random_state=1)
X_features = rbf_feature.fit_transform(x_train)
classifier5 = SGDClassifier(max_iter=100, tol=1.9)
classifier5.fit(x_train, y_train)
predictions5 = classifier5.predict(x_test)
matrix5 = confusion_matrix(y_test, predictions3)
print("kernel approximation accuracy: " + str(accuracy_score(y_test, predictions5)))
print("kernel approximation matrix: " + str(matrix5))

plt.bar(list(["decision tree", "k-neighbors", "naive bayer", "neural networks", "kernel approximation"]),
        [accuracy_score(y_test, predictions1), accuracy_score(y_test, predictions2), accuracy_score(y_test, predictions3), accuracy_score(y_test, predictions4), accuracy_score(y_test, predictions5)], color='g')
plt.show()
