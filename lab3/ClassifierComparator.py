from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier

from sklearn.metrics import accuracy_score

iris = datasets.load_iris()
x = iris.data
y = iris.target
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.4)

# klasyfikator 1: drzewo decyzyjne
# tworzenie drzewa na zbiorze treningowym
classifier1 = tree.DecisionTreeClassifier()
classifier1.fit(x_train, y_train)
# ewaluacja drzewa na zbiorze testowym
predictions = classifier1.predict(x_test)
print("decisiontre accuracy: " + str(accuracy_score(y_test, predictions)))

# klasyfikator 2: k-najblizszych sąsiadów
classifier2 = KNeighborsClassifier(n_neighbors=3)
classifier2.fit(x_train, y_train)
predictions = classifier2.predict(x_test)
print("k-neighbors accuracy: " + str(accuracy_score(y_test, predictions)))

# klasyfikator 3: naive bayer
classifier3 = GaussianNB()
classifier3.fit(x_train, y_train)
predictions = classifier3.predict(x_test)
print("naive bayer accuracy: " + str(accuracy_score(y_test, predictions)))

# klasyfikator 4: neural_networks
classifier4 = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 3), random_state=1)
classifier4.fit(x_train, y_train)
predictions = classifier4.predict(x_test)
print("neural networks accuracy: " + str(accuracy_score(y_test, predictions)))
