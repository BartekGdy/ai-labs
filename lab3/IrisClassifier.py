from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

iris = datasets.load_iris()

print(iris)
x = iris.data
y = iris.target
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.4)

print("x_train\n" + str(x_train))
print("x_test\n" + str(x_test))
print("y_train\n" + str(y_train))
print("y_test\n" + str(y_test))

def myIrisClassifier(db):
    predictions = []
    for db_record in db:
        if db_record[3] <= 0.6:
            predictions.append(0)
        elif db_record[3] > 1.7:
            predictions.append(2)
        else:
            predictions.append(1)
    return predictions

predictions = myIrisClassifier(x_test)
print("dokładność:" + str(accuracy_score(y_test,predictions)))