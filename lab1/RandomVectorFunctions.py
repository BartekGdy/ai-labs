from lab1 import VectorUtils as vu
import math

vector = vu.createVector(30, 0, 101)

minValue = min(vector)
maxValue = max(vector)
length = len(vector)
normalizedVector = vu.normalize(vector)
standarizedVector = []
sum = 0
for i in vector:
    sum += i
avarage = sum / length

temp = 0
for i in vector:
    temp += math.pow((i - avarage), 2)
standardDeviation = math.sqrt(temp / length)


vu.normalize(vector)

for i in vector:
    standarizedVector.append((i - avarage) / (standardDeviation))

print("Vector: " + str(vector))
print("Minimalna wartosc: " + str(minValue))
print("Posortowany rosnąco: " + str(sorted(vector, reverse=True)))
print("Srednia: " + str(avarage))
print("Odchylenie standardowe: " + str(standardDeviation))
print("Wektor znormalizowany: " + str(normalizedVector))
print("Wektor standaryzowany: " + str(standarizedVector))
