from lab1 import VectorUtils
import matplotlib.pyplot as plt

def countElements(arg):
    elements = {}
    for i in arg:
        if i in elements.keys():
            elements[i] += 1
        else:
            elements[i] = 1
    return elements

vector = VectorUtils.createVector(100, 0, 21)

elementsDict = countElements(vector)
print("Vector: " + str(vector))
print(str(elementsDict))

plt.bar(list(elementsDict.keys()), elementsDict.values(), color='g')
plt.show()