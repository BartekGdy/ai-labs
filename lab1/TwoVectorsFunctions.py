import math
from lab1 import VectorUtils


def calcScalarProduct(arg1, arg2):
    result = 0
    for i in range(len(arg1)):
        result += arg1[i] * arg2[i]
    return result

def calcVectorLen(arg):
    sum = 0
    for i in range(len(arg)):
        sum += math.pow(arg[i], 2)
    return math.sqrt(sum)


def calCosinus(arg1, arg2):
    return calcScalarProduct(arg1, arg2) / (calcVectorLen(arg1) * calcVectorLen(arg2))


def getAngleInRadians(arg1, arg2):
    return math.acos(calcScalarProduct(arg1, arg2) / (calcVectorLen(arg1) * calcVectorLen(arg2)))


def getAngleInDegrees(arg1, arg2):
    return math.degrees(getAngleInRadians(arg1, arg2))


vector1 = VectorUtils.createVector(10, 0, 21)
vector2 = VectorUtils.createVector(10, 0, 21)

scalarProduct = calcScalarProduct(vector1, vector2)
vectorLength1 = calcVectorLen(vector1)
vectorLength2 = calcVectorLen(vector2)
cosinusBetweenVector = calCosinus(vector1, vector2)
angleRadians = getAngleInRadians(vector1, vector2)
angleDegrees = getAngleInDegrees(vector1, vector2)

print("Vektor1: " + str(vector1))
print("Vektor2: " + str(vector2))
print("Iloczyn skalarny: " + str(scalarProduct))
print("Długość wektora1: " + str(vectorLength1))
print("Długość wektora2: " + str(vectorLength2))
print("Cosinus kata miedzy wektorami: " + str(cosinusBetweenVector))
print("Kąt miedzy wektorami w radianach: " + str(angleRadians))
print("Kąt miedzy wektorami w stopniach: " + str(angleDegrees))
