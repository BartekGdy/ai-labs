import random

def createVector(size, lowerBound, upperBound):
    result = []
    for x in range(0, size):
        result.append(random.randrange(lowerBound, upperBound))
    return result

def normalize(arg):
    minValue = min(arg)
    maxValue = max(arg)
    normalizedVector = []
    for i in arg:
        normalizedVector.append((i - minValue) / (maxValue - minValue))
    return normalizedVector
