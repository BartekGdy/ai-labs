import pandas as pd
from lab1 import VectorUtils as vu

dataset = pd.read_csv("cereal.csv")
print('Tabela płatków: ')
print(dataset)

names = dataset.loc[:, 'cereal_name']
print('Nazwy płatków: ')
print(names)

print('Typ Danych: ' + str(names.dtype))

dataset.drop(['manufacturer'], axis = 1, inplace = True)
print(dataset)

dataset['normalized_calories']= vu.normalize(dataset.loc[:, 'calories'])
print("Znormalizowane kalorie: ")
print((dataset.loc[:, 'normalized_calories']))
